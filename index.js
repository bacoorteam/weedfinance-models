module.exports = {
  StakeHistory: require('./models/StakeHistory'),
  Token: require('./models/Token'),
  StakeToken: require('./models/StakeToken'),
  Config: require('./models/Config'),
  TokenPercentageHistory: require('./models/TokenPercentageHistory'),
  UserAdmin: require('./models/UserAdmin'),
};
