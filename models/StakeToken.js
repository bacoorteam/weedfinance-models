'use strict';

const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
mongoose.Promise = global.Promise;
const Model = new Schema({
    address: {type: String, index: true},
    // name: String,
    // symbol: String,
    // decimals: Number,
    // icon: String,
    percent: Number,
    annualPercent: String,
    disabled: {type: Boolean, default: false},
    rewardAddress: {type: String, index: true},
    stakeAddress: {type: String, index: true},
    updatedAt: Date,
    createdAt: { type: Date, index: -1 }
});
Model.pre('save', function(next) {
    const now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});
Model.pre('update', function() {
    this.update({}, { $set: { updatedAt: new Date() } });
});
module.exports = mongoose.model('stake_token', Model);