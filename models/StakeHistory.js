'use strict';

const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
mongoose.Promise = global.Promise;
const Model = new Schema({
    hash: {type: String},
    userAddress: {type: String, index: true},
    tokenAddress: {type: String, index: true},
    rewardAddress: {type: String, index: true},
    amount: String,
    amountFormat: Number,
    action: {type: String, index: true}, // stake, unstake, claimReward
    updatedAt: Date,
    createdAt: { type: Date, index: -1 }
});
Model.index({ hash: 1, action: 1 }, { unique: true });
Model.pre('save', function(next) {
    const now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});
Model.pre('update', function() {
    this.update({}, { $set: { updatedAt: new Date() } });
});
module.exports = mongoose.model('stake_history', Model);