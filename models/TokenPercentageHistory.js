/**
 * Created by A on 7/18/17.
 */
'use strict';

const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;
mongoose.Promise = global.Promise;
const Model = new Schema({
    hash: {type: String, unique: true, index: true},
    tokenAddress: String,
    percent: Number,
    blockNumber: Number,
    createdAt: { type: Date },
    updatedAt: { type: Date }
});
Model.pre('save', function(next) {
    const now = new Date();
    if (!this.createdAt) {
        this.createdAt = now;
    }
    next();
});
Model.pre('update', function() {
    this.update({}, { $set: { updatedAt: new Date() } });
});
module.exports = mongoose.model('token_percentage_history', Model);